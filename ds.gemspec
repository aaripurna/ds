$:.push File.expand_path("lib", __dir__)

# Maintain your gem's version:
require "ds/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |spec|
  spec.name        = "ds"
  spec.version     = Ds::VERSION
  spec.authors     = ["nawa"]
  spec.email       = ["nap.aripurna@gmail.com"]
  spec.homepage    = "https://gitlab.com/aaripurna/ds"
  spec.summary     = "This is a resource generator for in asteroit cms style."
  spec.description = "This gem depends on asteroit form generator."
  spec.license     = "MIT"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata["allowed_push_host"] = "https://gitlab.com/aaripurna/ds"
  else
    raise "RubyGems 2.0 or newer is required to protect against " \
      "public gem pushes."
  end

  spec.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  spec.add_dependency "rails", "~> 6.0.0"

  spec.add_development_dependency "sqlite3"
  spec.add_development_dependency "pry-rails"
end
