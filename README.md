# Ds
This is a scaffolding tool that depends on asteroit form builder (I still don't publish it yet)

## Usage

``` bash
$ rails g <model name> entity:type --scope <some different scope>
```
it will generate

- app/models/model.rb
- app/controllers/name_controller.rb
- db/migrate/migrationn_name.rb
- app/views/dashboard/name/_form.html.haml
- app/views/dashboard/name/new.html.haml
- app/views/dashboard/name/edit.html.haml
- app/views/dashboard/name/index.html.haml
- app/presenters/name_presenter.rb

And also append the path to config/routes.rb

## Installation
Add this line to your application's Gemfile:

```ruby
gem 'ds', '~> 0.1.2', git: 'https://gitlab.com/aaripurna/ds.git'
```

And then execute:
```bash
$ bundle
```

## Contributing
Just fork this and make your own

## License
The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
