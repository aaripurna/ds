module Dashboard
  class BoosController < DashboardController
    before_action :set_boo, except: [:create, :new, :index]

    def index
      @boos = Boo.all
    end

    def new
      @boo = Boo.new
    end

    def edit
    end

    def show
      redirect_to action: :edit
    end

    def create
      @boo = Boo.new(boo_params)
      respond_to do |format|
        if @boo.save
          format.html { redirect_to action: :index, notice: "boo created successfully!" }
          format.json { render json: { location: dashboard_boos_url }, status: :created }
        else
          format.html { redirect_to action: :new, notice: "failed to create boo" }
          format.json { render json: @boo.errors, status: :unprocessable_entity }
        end
      end
    end

    def update
      respond_to do |format|
        if @boo.update(boo_params)
          format.html { redirect_to action: :index, notice: "boo updated successfully!" }
          format.json { render json: { location: dashboard_boos_url }, status: :created }
        else
          format.html { redirect_to action: :edit, notice: "failed to update boo" }
          format.json { render json: @boo.errors, status: :unprocessable_entity }
        end
      end
    end

    def destroy
      respond_to do |format|
        if @boo.destroy
          format.html { redirect_to action: :index, notice: "boo deleted successfully!" }
          format.json { render json: { location: dashboard_boos_url }, status: :ok }
        else
          format.html { redirect_to action: :index, notice: "failed to delete boo" }
          format.json { render json: @boo.errors, status: :unprocessable_entity }
        end
      end
    end

    private

    def set_boo
      @boo = Boo.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      redirect_to action: :index
    else
      objectize(@boo)
    end

    def boo_params 
      params.require(:boo).permit(:name)
    end

  end
end
