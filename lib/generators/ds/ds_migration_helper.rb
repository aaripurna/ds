module Ds
  module Generators
    module DsMigrationHelper
      def migration_class_name
        "Create#{model_name.pluralize}" 
      end

      def migration_file_name
        migration_class_name.underscore
      end

      def table_name
        @file_name.downcase.underscore.pluralize
      end

      def migration_attributes
        methods.map do |method|
          name, type, argument = method.split(':')
          options = []
          row = entity_aliases[type]
          
          next if ( ['file', 'image', 'galleries'].include?(argument) || row['field'].blank?)

          if (type == 'references' || row['field'] == 'references') && argument&.include?('polymorphic')
            options << "polymorphic: true" 

          elsif type == 'references' || row['field'] == 'references' 
            options << "index: true"
            options << "foreign_key: true"

          elsif argument&.include? ('index')
            options << "index: true" 
          end

          options << "array: true" if (row['multiple'].present? || ['multiple', 'array'].include?(argument))
          
          if argument.present?
            options << "unique: true" if (argument.include?('unique') && !(type == 'references' || row['field'] == 'references'))
          end
          {name: name, field: row['field'], options: options}
        end
      end

      def migration_file_path
        Rails.root.join("db/migrate/#{timestamp}_#{migration_file_name}.rb")
      end
    end
  end
end
