require 'yaml'
require 'time'

module Ds
  module Generators
    module DsHelper
      def entity_aliases
        @entity_aliases ||= YAML.load_file(File.expand_path("../entity_aliases.yml", __FILE__))
      end

      def timestamp
        Time.now.strftime("%Y%m%d%H%M%S")
      end

      def model_name
        @file_name.singularize.camelize
      end

      def model_file_name
        model_name.downcase
      end

      def model_path
        "#{model_root}#{model_file_name}.rb"
      end

      def presenter_path
        Rails.root.join("app/presenter/#{singular_name.underscore}_presenter.rb")
      end

      def controller_path
        "#{dashboard_controller_root}#{controller_file_name}.rb"
      end
      
      def model_root
        Rails.root.join('app/models/')
      end

      def attachments_to_model
        files = []
        methods.each do |entity|
          name, type, argument = entity.split(':')
          row = entity_aliases[type]
          if row['type'] == 'file' || type == 'file'
            file = (row['multiple'].present? || ['array', 'multiple'].include?(argument)) ? "has_many_attached :#{name}" : "has_one_attached :#{name}"
            files.push file
          end
          if type == 'references' || row['field'] == 'references'
            files.push("belongs_to :#{name}")
          end
        end
        files
      end
    end
  end
end
