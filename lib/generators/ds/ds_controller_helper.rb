module Ds
	module Generators
    module DsControllerHelper
      def controller_class_name
        "#{@file_name.pluralize.camelize}Controller"
      end

      def controller_file_name
        "#{@file_name.pluralize.underscore}_controller"
      end

      def dashboard_controller_root
        Rails.root.join('app/controllers/dashboard/')
      end

      def single_params
        parameterize_methods.select { |param| param.exclude?("[]")}
      end

      def multiple_params
        parameterize_methods.select { |param| param.include?("[]")}
      end

      def params_to_permit
        single_params.concat(multiple_params).join(', ')
      end

      def plural_index
        "@#{plural_name.underscore} = #{model_name}.all"
      end

      def index_to_show
        single? ? singular_index : plural_index
      end

      def singular_index
        objec_name = singular_name.underscore
        %Q|@#{objec_name} = #{model_name}.first
          if @#{objec_name}.present?
            redirect_to dashboard_#{objec_name}_path(@#{objec_name})
          else
            redirect_to action: :new
          end|
      end

      def setter_to_show
        single? ? singular_setter : plural_setter 
      end

      def singular_setter
        {finder: "#{model_name}.first!", redirect: "action: :new"}
      end

      def plural_setter
        {finder: "#{model_name}.find(params[:id])", redirect: "action: :index"}
      end

      def attachable_galleries
        galleries = []
        methods.map do |entity|
          name, type, argument = entity.split(':')
          row = entity_aliases[type]
          if ['galleries', 'gallery'].include?(type) || row['form'] == 'galleries'
            galleries.push(name)
          end
        end
        galleries
      end

      def parameterize_methods
        methods.map do |entity|
          name, type, argument = entity.split(':')
          row = entity_aliases[type]
          if row['multiple'].present? || ['array', 'multiple'].include?(argument) 
            "#{name}: []"
          else
            ":#{name}"
          end
        end
      end
    end
  end
end
