module Ds
  module Generators
    module DsViewHelper
      def view_files
        single? ? %w(new edit _form) : %w(index new edit _form)
      end

      def dashboard_view_path(file)
        "#{dashboard_view_root}#{plural_name.underscore}/#{file}"
      end

      def dashboard_view_root
        Rails.root.join('app/views/dashboard/')
      end

      def input_forms
        methods.map do |method|
          name, type, argument = method.split(':')
          label = name
          row = entity_aliases[type]
          if row['field'] == 'references' || type == 'references'
            name = "#{name}_id"
          end
          {name: name, input: row['form'], lable: label}
        end
      end

      def single?
        options['scope']&.include?('single')
      end

      def client_api?
        options['scope']&.include?('client-api')
      end

      def api?
        options['scope']&.include?('api')
      end

      def no_view?    
        options['scope']&.include?('no-view')
      end

      def generate_dashboard_view?
        !no_view? || client_api?
      end

      def resource_api
        valid_scope = {
          api: {
            root: "api",
            controller: "controller.erb",
            views: %w(_partial index show),
            controller_path: Rails.root.join("app/controllers/dashboard/dashboard_api/"),
            view_path: Rails.root.join("app/views/dashboard/dashboard_api/"),
            route: "scope module: :dashboard_api, path: 'dashboard/api/', defaults: {format: :json} do"
          },client_api: {
            root: "client_api",
            controller: "controller.erb",
            views: [],
            controller_path: Rails.root.join("app/controllers/client/client_api"),
            route: "scope module: :client_api, path: 'api/', defaults: {format: :json} do"
          }
        }
        if client_api?
          return valid_scope[:client_api]
        elsif api?
          return valid_scope[:api]
        end
      end

    end

  end
end
