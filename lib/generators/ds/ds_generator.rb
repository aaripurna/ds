require File.expand_path('ds_controller_helper.rb', __dir__)
require File.expand_path('ds_helper.rb', __dir__)
require File.expand_path('ds_view_helper.rb', __dir__)
require File.expand_path('ds_migration_helper.rb', __dir__)

module Ds
	module Generators
		class DsGenerator < ::Rails::Generators::NamedBase

      include Ds::Generators::DsHelper
      include Ds::Generators::DsControllerHelper
      include Ds::Generators::DsViewHelper
      include Ds::Generators::DsMigrationHelper
			
      source_root File.expand_path('templates', __dir__)
			class_option :module, type: :string
			class_option :scope, type: :array, default: []
			argument :methods, type: :array, default: []
			
			def validate_arguments
        validate_file_name
        validate_field_and_type
      end

      def generate_view_files
			end

      def create_view_files
        return unless generate_dashboard_view?
        if resource_api.present?
          resource_api[:views].each do |view|
            generated_view = view == '_partial' ? "_#{singular_name}" : view
            template "#{resource_api[:root]}/views/#{view}.erb", "#{resource_api[:view_path]}/#{generated_view}.json.jbuilder"
          end
        else
          view_files.each do |file_name|
            template "views/#{file_name}.erb", dashboard_view_path("#{file_name}.html.haml")
          end
        end
      end

      def create_controller_file
        if resource_api.present?
          template "#{resource_api[:root]}/controller.erb", "#{resource_api[:controller_path]}/#{controller_file_name}.rb"
        else
          template 'controller.erb', controller_path
        end
      end

      def create_model_file
        template 'model.erb', model_path
      end

      def create_presenter_file
        template 'presenter.erb', presenter_path
      end

      def create_migration_file
        template 'migration.erb', migration_file_path
      end

      def append_routes
        if resource_api.present?
          inject_into_file Rails.root.join('config/routes.rb'), "\n\t\t\tresources :#{plural_name.underscore}", after: resource_api[:route]
        else
          inject_into_file Rails.root.join('config/routes.rb'), "\n\t\tresources :#{plural_name.underscore}, concerns: :detachable", after: 'namespace :dashboard do'
        end
      end
      
      private

      def validate_file_name
        raise(NameError, 'Invalid file name') if file_name.match(/(^\d)|[^\w]/)
      end

      def validate_field_and_type
        methods.each do |method|
          arguments = method.split(":")
          arguments.each do |field|
            raise(NameError, 'Invalid field or type name') if field.match(/(^\d)|[^\w]/)
          end
        end
      end
		end
	end
end
